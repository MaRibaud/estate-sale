# README #


### What is this repository for? ###

A top down 2D game where the player goes around defending himself from the undead 
staff of the once great Lorque Estate

2019.4.18


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Resources ###

Unity 2D - Brackeyes - https://www.youtube.com/playlist?list=PLPV2KyIb3jR6TFcFuzI2bB7TMNIIBpKMQ

Estate Sale HnP - https://app.hacknplan.com/p/143801/kanban?categoryId=0&boardId=378672&taskId=16&tabId=basicinfo

### Contact ###
Mark Ribaudo 

maribaud@iu.edu 
